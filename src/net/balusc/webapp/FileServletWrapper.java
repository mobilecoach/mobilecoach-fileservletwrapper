package net.balusc.webapp;

/*
 * net/balusc/webapp/FileServletWrapper.java
 *
 * © 2013-2017 Center for Digital Health Interventions, Health-IS Lab a joint
 * initiative of the Institute of Technology Management at University of St.
 * Gallen and the Department of Management, Technology and Economics at ETH
 * Zurich
 *
 * For details see README.md file in the root folder of this project.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * MobileCoach Wrapper for {@link FileServlet}
 *
 * @author Andreas Filler
 */
public class FileServletWrapper {

	private FileServlet fileServlet = null;

	public FileServletWrapper() {
		fileServlet = new FileServlet();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see net.balusc.webapp.FileServlet#init()
	 */
	public void init(final ServletContext servletContext)
			throws ServletException {
		fileServlet.init(new ServletConfig() {
			@Override
			public String getServletName() {
				return "Wrapped File Servlet";
			}

			@Override
			public ServletContext getServletContext() {
				return servletContext;
			}

			@Override
			public Enumeration<String> getInitParameterNames() {
				final String[] parameterNames = new String[] { "basePath" };
				return makeEnumeration(parameterNames);
			}

			@Override
			public String getInitParameter(final String parameter) {
				if (parameter.equals("basePath")) {
					return "";
				} else {
					return null;
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see net.balusc.webapp.FileServlet#doHead(javax.servlet.http.
	 * HttpServletRequest , javax.servlet.http.HttpServletResponse)
	 */
	public void doHead(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		fileServlet.doHead(request, response);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * net.balusc.webapp.FileServlet#doGet(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(final HttpServletRequest request,
			final HttpServletResponse response)
			throws ServletException, IOException {
		fileServlet.doGet(request, response);
	}

	/**
	 * Helper method to create String enumeration
	 *
	 * @param obj
	 * @return
	 */
	private Enumeration<String> makeEnumeration(final Object obj) {
		return new Enumeration<String>() {
			int	size	= Array.getLength(obj);

			int	cursor;

			@Override
			public boolean hasMoreElements() {
				return cursor < size;
			}

			@Override
			public String nextElement() {
				return (String) Array.get(obj, cursor++);
			}
		};
	}
}