# Welcome to the *MobileCoach*!

*MobileCoach* is the Open Source Behavioral Intervention Platform designed by **ETH Zurich**, the **University of St. Gallen** and the **Swiss Research Institute for Public Health and Addiction**.

© 2013-2017 [Center for Digital Health Interventions, Health-IS Lab](http://www.c4dhi.org) a joint initiative of the [Institute of Technology Management](http://www.item.unisg.ch) at [University of St. Gallen](http://www.unisg.ch) and the [Department of Management, Technology and Economics](http://mtec.ethz.ch) at [ETH Zurich](http://www.ethz.ch).    

For further information visit the *MobileCoach* Website at [https://www.mobile-coach.eu](https://www.mobile-coach.eu)!

## Team of the release version

### Center for Digital Health Interventions, Health-IS Lab

* **Andreas Filler** - afiller (AT) ethz (DOT) ch
* **Dr. Tobias Kowatsch** - tobias (DOT) kowatsch (AT) unisg (DOT) ch
* **Jost Schweinfurther** - jschweinfurther (AT) ethz (DOT) ch
* **Prof. Dr. Elgar Fleisch** - efleisch (AT) ethz (DOT) ch

### Swiss Research Institute for Public Health and Addiction

* **Dr. Severin Haug** - severin (DOT) haug (AT) isgf (DOT) uzh (DOT) ch
* **Raquel Paz Castro** - raquel (DOT) paz (AT) isgf (DOT) uzh (DOT) ch

## License

License information can be found in the [LICENSE.txt](LICENSE.txt) file in the root folder of this project.

---

# *MobileCoach FileServletWrapper* usage information

The *MobileCoach FileServletWrapper* is an optional component for the MobileCoach system. To be used, it needs to be added to the library path of the *MobileCoach* system, e.g. by copying the created **.jar** file to the **WEB-INF/lib** folder. This process is performed automatically by the *MobileCoach* build process using the **build.xml** provided with the *MobileCoach* core package.

**Suggestion:** Use the provided **build.xml** within **Eclipse** to create the **.jar** file.

## Prerequisites

* Java (8 or newer) SDK 
* Optional, but suggested: Eclipse WTP (Project files included in repository)

## Further information

For further information have a look at the **README.md** file in the main repository of the *MobileCoach*.

### Version 1.0.4 (Build 20171129)